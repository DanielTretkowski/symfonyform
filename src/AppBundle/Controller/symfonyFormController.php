<?php

namespace AppBundle\Controller;

use AppBundle\Entity\symfonyForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Symfonyform controller.
 *
 * @Route("/")
 */
class symfonyFormController extends Controller
{
    /**
     * Lists all symfonyForm entities.
     *
     * @Route("/", name="symfonyform_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $symfonyForms = $em->getRepository('AppBundle:symfonyForm')->findAll();

        return $this->render('symfonyform/index.html.twig', array(
            'symfonyForms' => $symfonyForms,
        ));
    }

    /**
     * Creates a new symfonyForm entity.
     *
     * @Route("/new", name="symfonyform_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $symfonyForm = new Symfonyform();
        $form = $this->createForm('AppBundle\Form\symfonyFormType', $symfonyForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($symfonyForm);
            $em->flush();

            return $this->redirectToRoute('symfonyform_show', array('id' => $symfonyForm->getId()));
        }

        return $this->render('symfonyform/new.html.twig', array(
            'symfonyForm' => $symfonyForm,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a symfonyForm entity.
     *
     * @Route("/{id}", name="symfonyform_show")
     * @Method("GET")
     */
    public function showAction(symfonyForm $symfonyForm)
    {
        $deleteForm = $this->createDeleteForm($symfonyForm);

        return $this->render('symfonyform/show.html.twig', array(
            'symfonyForm' => $symfonyForm,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing symfonyForm entity.
     *
     * @Route("/{id}/edit", name="symfonyform_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, symfonyForm $symfonyForm)
    {
        $deleteForm = $this->createDeleteForm($symfonyForm);
        $editForm = $this->createForm('AppBundle\Form\symfonyFormType', $symfonyForm);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('symfonyform_edit', array('id' => $symfonyForm->getId()));
        }

        return $this->render('symfonyform/edit.html.twig', array(
            'symfonyForm' => $symfonyForm,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a symfonyForm entity.
     *
     * @Route("/{id}", name="symfonyform_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, symfonyForm $symfonyForm)
    {
        $form = $this->createDeleteForm($symfonyForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($symfonyForm);
            $em->flush();
        }

        return $this->redirectToRoute('symfonyform_index');
    }

    /**
     * Creates a form to delete a symfonyForm entity.
     *
     * @param symfonyForm $symfonyForm The symfonyForm entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(symfonyForm $symfonyForm)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('symfonyform_delete', array('id' => $symfonyForm->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
