<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * symfonyForm
 *
 * @ORM\Table(name="symfony_form")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\symfonyFormRepository")
 */
class symfonyForm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="surName", type="string", length=255)
     */
    private $surName;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="experience", type="integer")
     */
    private $experience;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Country()
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * @var array
     * @Assert\NotBlank()
     * @ORM\Column(name="availability", type="json_array", length=255)
     */
    private $availability;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="profession", type="string", length=255)
     */
    private $profession;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return symfonyForm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surName
     *
     * @param string $surName
     *
     * @return symfonyForm
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;

        return $this;
    }

    /**
     * Get surName
     *
     * @return string
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * Set experience
     *
     * @param integer $experience
     *
     * @return symfonyForm
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return int
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return symfonyForm
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return symfonyForm
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set availability
     *
     * @param string $availability
     *
     * @return symfonyForm
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return symfonyForm
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }
}

